import java.util.ArrayList;

public class Point {
    public double latitude;
    public double longitude;
    public int orderNumber;

    public Point(){

    }
    public Point(double latitude, double longitude, int orderNumber) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.orderNumber = orderNumber;
    }
}
