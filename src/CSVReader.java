import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReader {
    public static List<List<String>> read(String csvFileName) throws IOException {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
        }
        return records;
    }

    public static List<Shop> prepareShops() throws IOException {
        List<List<String>> shopsList = read("shops.csv");
        List<Shop> allShops = new ArrayList<>();

        for (int i = 0; i < shopsList.size(); i++) {
            Shop s = new Shop(new ArrayList<>(), shopsList.get(i).get(0),
                    new Point(Double.parseDouble(shopsList.get(i).get(1)),
                            Double.parseDouble(shopsList.get(i).get(2)),
                            0),
                    Integer.parseInt(shopsList.get(i).get(3)),
                    Integer.parseInt(shopsList.get(i).get(4)));
            allShops.add(s);
        }

        return allShops;
    }

    public static List<Point> preparePoints() throws IOException {
        List<List<String>> pointsList = read("points.csv");
        List<Point> allPoints = new ArrayList<>();

        for (int i = 0; i < pointsList.size(); i++) {
            Point p = new Point(Double.parseDouble(pointsList.get(i).get(1)),
                    Double.parseDouble(pointsList.get(i).get(2)),
                    Integer.parseInt(pointsList.get(i).get(0)));
            allPoints.add(p);

        }

        return allPoints;
    }
}

