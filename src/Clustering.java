import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.*;

public class Clustering {

    public static Map<Integer, Double> kHash = new HashMap<Integer, Double>();
    public static Map<Integer, Double> yHash = new HashMap<Integer, Double>();
    public static Map<Integer, Double> mHash = new HashMap<Integer, Double>();
    public static Map<Integer, Double> kSorted = new LinkedHashMap<>();
    public static Map<Integer, Double> ySorted = new LinkedHashMap<>();
    public static Map<Integer, Double> mSorted = new LinkedHashMap<>();
    public static Map<Integer, Double> kSorted20 = new LinkedHashMap<>();
    public static Map<Integer, Double> mSorted20 = new LinkedHashMap<>();
    public static Map<Integer, Double> ySorted35 = new LinkedHashMap<>();
    public static Set<Integer> sM20K20;
    public static Set<Integer> sK20Y35;
    public static Set<Integer> sY35M20;
    public static List<Shop> allMyShops;
    public static List<Point> allMyPoints;
    public static ArrayList<Point> remainingPoints;

    public static double distanceInKm(Point source, Point target) {
        if ((source.latitude == target.latitude) && (source.longitude == target.longitude)) {
            return 0;
        } else {
            double theta = source.longitude - target.longitude;
            double dist = Math.sin(Math.toRadians(source.latitude))
                    * Math.sin(Math.toRadians(target.latitude))
                    + Math.cos(Math.toRadians(source.latitude))
                    * Math.cos(Math.toRadians(target.latitude))
                    * Math.cos(Math.toRadians(theta));

            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;

            return (dist);
        }
    }

    public static void createHashes(List<Shop> allShops, List<Point> allPoints) {
        allMyShops = allShops;
        allMyPoints = allPoints;
        for (Shop s : allShops) {
            if (s.shopName.equals("K")) {
                for (Point p : allPoints) {
                    kHash.put(p.orderNumber, distanceInKm(p, s.point));
                }
            } else if (s.shopName.equals("Y")) {
                for (Point p : allPoints) {
                    yHash.put(p.orderNumber, distanceInKm(p, s.point));
                }
            } else if (s.shopName.equals("M")) {
                for (Point p : allPoints) {
                    mHash.put(p.orderNumber, distanceInKm(p, s.point));
                }
            } else {
                System.err.println("Sistemde olmayan bir merkez!");
            }
        }
        sortHashes();
    }

    public static void sortHashes() {
        kHash.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEachOrdered(x -> kSorted.put(x.getKey(), x.getValue()));
        yHash.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEachOrdered(x -> ySorted.put(x.getKey(), x.getValue()));
        mHash.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEachOrdered(x -> mSorted.put(x.getKey(), x.getValue()));
        findSamePoints();
    }

    public static void findSamePoints() {
        int count = 0;

        for (Map.Entry<Integer, Double> item : kSorted.entrySet()) {

            if (count < 20) {
                kSorted20.put(item.getKey(), item.getValue());
            }
            count++;
        }
        count = 0;

        for (Map.Entry<Integer, Double> item : ySorted.entrySet()) {

            if (count < 35) {
                ySorted35.put(item.getKey(), item.getValue());
            }
            count++;
        }
        count = 0;

        for (Map.Entry<Integer, Double> item : mSorted.entrySet()) {

            if (count < 20) {
                mSorted20.put(item.getKey(), item.getValue());
            }
            count++;
        }


        retainAll();
        test();

    }

    public static void retainAll() {
        sM20K20 = new HashSet<Integer>(kSorted20.keySet());
        sM20K20.retainAll(mSorted20.keySet());


        sK20Y35 = new HashSet<Integer>(kSorted20.keySet());
        sK20Y35.retainAll(ySorted35.keySet());


        sY35M20 = new HashSet<Integer>(ySorted35.keySet());
        sY35M20.retainAll(mSorted20.keySet());
    }

    public static void test() {
        List<Integer> keysK = new ArrayList(kSorted.keySet());
        List<Integer> keysM = new ArrayList(mSorted.keySet());
        List<Integer> keysY = new ArrayList(ySorted.keySet());

        List<Double> valuesK = new ArrayList(kSorted.values());
        List<Double> valuesM = new ArrayList(mSorted.values());
        List<Double> valuesY = new ArrayList(ySorted.values());


        if (sM20K20.size() > 0) {
            for (Integer i : sM20K20) {
                keysK = new ArrayList(kSorted.keySet());
                keysM = new ArrayList(mSorted.keySet());
                keysY = new ArrayList(ySorted.keySet());

                valuesK = new ArrayList(kSorted.values());
                valuesM = new ArrayList(mSorted.values());
                valuesY = new ArrayList(ySorted.values());
                double k21Value = kSorted.get(keysK.get(21));
                double m21Value = mSorted.get(keysM.get(21));
                if (k21Value > m21Value) {
                    mSorted20.remove(i);
                    mSorted20.put(keysM.get(21), valuesM.get(21));
                    mSorted.remove(keysM.get(21));
                } else {
                    kSorted20.remove(i);
                    kSorted20.put(keysK.get(21), valuesK.get(21));
                    kSorted.remove(keysK.get(21));
                }
            }
        }
        if (sK20Y35.size() > 0) {
            for (Integer i : sK20Y35) {
                keysK = new ArrayList(kSorted.keySet());
                keysM = new ArrayList(mSorted.keySet());
                keysY = new ArrayList(ySorted.keySet());

                valuesK = new ArrayList(kSorted.values());
                valuesM = new ArrayList(mSorted.values());
                valuesY = new ArrayList(ySorted.values());
                double k21Value = kSorted.get(keysK.get(21));
                double y36Value = mSorted.get(keysM.get(36));
                if (k21Value > y36Value) {
                    ySorted35.remove(i);
                    ySorted35.put(keysY.get(36), valuesY.get(36));
                    ySorted.remove(keysY.get(36));
                } else {
                    kSorted20.remove(i);
                    kSorted20.put(keysK.get(21), valuesK.get(21));
                    kSorted.remove(keysK.get(21));
                }
            }
        }
        if (sY35M20.size() > 0) {
            for (Integer i : sY35M20) {
                keysK = new ArrayList(kSorted.keySet());
                keysM = new ArrayList(mSorted.keySet());
                keysY = new ArrayList(ySorted.keySet());

                valuesK = new ArrayList(kSorted.values());
                valuesM = new ArrayList(mSorted.values());
                valuesY = new ArrayList(ySorted.values());
                double m21Value = mSorted.get(keysM.get(21));
                double y36Value = ySorted.get(keysY.get(36));
                if (m21Value > y36Value) {
                    ySorted35.remove(i);
                    ySorted35.put(keysY.get(36), valuesY.get(36));
                    ySorted.remove(keysY.get(36));
                } else {
                    mSorted20.remove(i);
                    mSorted20.put(keysM.get(21), valuesM.get(21));
                    mSorted20.remove(keysM.get(21));
                }
            }
        }

        retainAll();

        if (sM20K20.size() != 0 || sY35M20.size() != 0 || sK20Y35.size() != 0) {
            test();
        } else {
            for (Map.Entry<Integer, Double> lhm : kSorted20.entrySet()) {
                for (Point p : allMyPoints) {
                    if (lhm.getKey() == p.orderNumber) {
                        allMyShops.get(0).initialPointsAccordingToMinimum.add(p);
                    }
                }
            }
            for (Map.Entry<Integer, Double> lhm : mSorted20.entrySet()) {
                for (Point p : allMyPoints) {
                    if (lhm.getKey() == p.orderNumber) {
                        allMyShops.get(2).initialPointsAccordingToMinimum.add(p);
                    }
                }
            }
            for (Map.Entry<Integer, Double> lhm : ySorted35.entrySet()) {
                for (Point p : allMyPoints) {
                    if (lhm.getKey() == p.orderNumber) {
                        allMyShops.get(1).initialPointsAccordingToMinimum.add(p);
                    }
                }
            }

            findUnassignedPoints();

            for (Point p : remainingPoints) {
                double distanceToK = distanceInKm(p, allMyShops.get(0).point);
                double distanceToY = distanceInKm(p, allMyShops.get(1).point);
                double distanceToM = distanceInKm(p, allMyShops.get(2).point);
                boolean kMax = allMyShops.get(0).initialPointsAccordingToMinimum.size() < 30;
                boolean yMax = allMyShops.get(1).initialPointsAccordingToMinimum.size() < 50;
                boolean mMax = allMyShops.get(2).initialPointsAccordingToMinimum.size() < 80;

                if (distanceToK > distanceToM && distanceToY > distanceToM
                        && mMax) {
                    allMyShops.get(2).initialPointsAccordingToMinimum.add(p);
                } else if (distanceToM > distanceToK && distanceToY > distanceToK
                        && kMax) {
                    allMyShops.get(0).initialPointsAccordingToMinimum.add(p);
                } else if (distanceToK > distanceToY && distanceToM > distanceToY
                        && yMax) {
                    allMyShops.get(1).initialPointsAccordingToMinimum.add(p);
                } else {
                    if (!yMax) {
                        if (distanceToK > distanceToM) {
                            allMyShops.get(2).initialPointsAccordingToMinimum.add(p);
                        } else {
                            allMyShops.get(0).initialPointsAccordingToMinimum.add(p);
                        }
                    }
                    if (!kMax) {
                        if (distanceToY > distanceToM) {
                            allMyShops.get(2).initialPointsAccordingToMinimum.add(p);
                        } else {
                            allMyShops.get(1).initialPointsAccordingToMinimum.add(p);
                        }
                    }
                    if (!mMax) {
                        if (distanceToK > distanceToY) {
                            allMyShops.get(1).initialPointsAccordingToMinimum.add(p);
                        } else {
                            allMyShops.get(0).initialPointsAccordingToMinimum.add(p);
                        }
                    }

                }
            }
            print();
        }
    }

    public static void findUnassignedPoints() {
        ArrayList<Point> allAssignedPoints = new ArrayList<>();
        allAssignedPoints.addAll(allMyShops.get(0).initialPointsAccordingToMinimum);
        allAssignedPoints.addAll(allMyShops.get(1).initialPointsAccordingToMinimum);
        allAssignedPoints.addAll(allMyShops.get(2).initialPointsAccordingToMinimum);

        remainingPoints = new ArrayList<>();

        remainingPoints.addAll(allMyPoints);

        for (Point p : allAssignedPoints) {
            if (allMyPoints.contains(p)) {
                remainingPoints.remove(p);
            }
        }

    }

    public static void print() {
        int index = 0;

        for (Shop s : allMyShops) {
            if (s.shopName.equals("Y"))
                index = 1;
            if (s.shopName.equals("M"))
                index = 2;

            try (PrintWriter writer = new PrintWriter(new File(s.shopName + ".csv"))) {
                StringBuilder sb = new StringBuilder();
                for (Point p : allMyShops.get(index).initialPointsAccordingToMinimum) {
                    sb.append(p.orderNumber);
                    sb.append(',');
                    sb.append(p.latitude);
                    sb.append(',');
                    sb.append(p.longitude);
                    sb.append('\n');
                }
                writer.write(sb.toString());

            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
            }

        }
        calculateShopRouting();
    }

    public static void nearestNeighbor(int shopId) {

        ArrayList<Point> shopPoints = new ArrayList<>();
        shopPoints.addAll(allMyShops.get(shopId).initialPointsAccordingToMinimum);
        double kMin = 10000.0;
        double ktotalDistance = 0.0;
        Point selectedPoint = allMyShops.get(shopId).point;
        Point oldSelected = allMyShops.get(shopId).point;
        ArrayList<Point> routing = new ArrayList<>();

        while (shopPoints.size() > 1) {
            for (Point p : shopPoints) {
                double distance = distanceInKm(selectedPoint, p);
                if (kMin >= distance) {
                    oldSelected = selectedPoint;
                    kMin = distance;
                    selectedPoint = p;
                    routing.add(oldSelected);
                }
            }

            ktotalDistance += kMin;
            shopPoints.remove(oldSelected);
            kMin = 10000.0;

        }
        String routingResult = "" + allMyShops.get(shopId).shopName + " icin siralama : ";
        for (Point p : routing) {
            if (!routingResult.contains(Integer.toString(p.orderNumber))) {
                if (p.orderNumber == 0) {
                    routingResult = routingResult + " --> " + allMyShops.get(shopId).shopName;
                } else {
                    routingResult = routingResult + " --> " + p.orderNumber;
                }

            }

        }

        ktotalDistance += distanceInKm(shopPoints.get(0), allMyShops.get(shopId).point);

        shopPoints.remove(shopPoints.get(0));

        try (PrintWriter writer = new PrintWriter(new File("RESULT" + allMyShops.get(shopId).shopName + ".txt")y)) {
            StringBuilder sb = new StringBuilder();
            sb.append(routingResult);
            sb.append("\n");
            sb.append(allMyShops.get(shopId).shopName + " icin alinan toplam yol : " + ktotalDistance + " KM ");
            writer.write(sb.toString());

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }

    }

    public static void calculateShopRouting() {
        nearestNeighbor(0);
        nearestNeighbor(1);
        nearestNeighbor(2);
    }


}
















