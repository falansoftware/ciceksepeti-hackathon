import java.util.ArrayList;

public class Shop {
    public ArrayList<Point> initialPointsAccordingToMinimum;
    public String shopName;
    public Point point;
    public int min;
    public int max;


    public Shop(ArrayList<Point> initialPointsAccordingToMinimum, String shopName, Point point, int min, int max) {
        this.initialPointsAccordingToMinimum = initialPointsAccordingToMinimum;
        this.shopName = shopName;
        this.point = point;
        this.max = max;
        this.min = min;
    }
}


