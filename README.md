Algoritmanın çalışma şekli şu şekilde;

------VERİ OKUMA------
1.Excelden gelen verileri daha rahat okumak için 2 ye bölüp csv formatında okuma yaptık.
------VERİ OKUMA------


-------KÜMELEME-------

1.Her çiçek bayisinin 100 noktaya olan uzaklığını buluyoruz.

2.Her çiçek bayisinin minimun dağıtması gereken kapasitelere göre  bayilerin noktalara olan uzaklıklarını sıralayıp, minimun değer kadarını alıyoruz.

3.3 bayininde minimunları doldu 75 noktamız atandı. Ama çakışan yani 1 den fazla bayiye düşen noktalar var.

4.Çakışan noktaları bulup, çakıştığı 2 bayi arasında yol maliyeti en düşük şekilde olcak seviyede atıyoruz.

5.Bayiler arasında hiç ortak nokta kalmayana kadar bu işlemi tekrarlayıp 75 noktayı bayilere minimun olacak şekilde atadık (20+20+35).

6.Kalan 25 noktayıda teker teker çiçek bayilerine uzaklıklarını bulup, bayilerin kapasitesi taşmıyacak şekilde dağıtıyoruz.

7.100 noktada bittikten sonra her bayinin minimumdan çok maksimumdan az dağıtması gereken nokta bulunuyor.

-------KÜMELEME-------


-------KÜMELEME GÖSTERİMİ--------

1.Kümeleme bittiğinde ise her bayinin gideceği noktaları ayrı csv dosyalarına çıkarıp google da gösteriyoruz.

2.Harita linki: https://drive.google.com/open?id=1MGlwJGCTzffPd4VciLtdQcbOz_yeBbMF&usp=sharing

-------KÜMELEME GÖSTERİMİ--------


-------ROTA HESABI-------
1.Şu an elimizde her çiçek bayisinin gitmesi gereken belli sayıda sipariş noktası var.

2.Herhangi bir bayi için gidilmesi gereken en yakın noktaya gidip işleme başlıyoruz.

3.Seçilen en yakın noktaya da en yakın noktaya giderek, çiçek bayisinin dağıtması gereken tüm siparişleri dağıtıyoruz ve yol maliyetlerini km cinsinden kuş uçuşu topluyoruz.

4.Tüm noktalar tamamlandığında son dağıtılan sipariş noktasının, çiçek bayisine olan uzaklığını buluyoruz. Bunu da toplam yola ekleyip o bayinin o günkü toplam yol maliyetini bulmuş oluyoruz.

5.Her bayinin tüm yol maliyetlerini ve gidilen rotanın sırasını txt dosyasına yazıp çıkarıyoruz.
-------ROTA HESABI-------

